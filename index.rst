.. SPDX-FileCopyrightText: Huawei Inc.
..
.. SPDX-License-Identifier: CC-BY-4.0

.. include:: definitions.rst

|main_project_name| Documentation
#############################################

Welcome to Oniro Project documentation! 

You are welcome to take a tour and play with Oniro's initial code contribution.
It is in the final stage toward becoming the project's official codebase.
If you feel like joining, we would love to have you among the list of Oniro's
initiating supporters. These are exciting times! There couldn't be a better
moment for joining Oniro!

To learn more about the |main_project_name|, go to https://oniroproject.org.

.. note::
   Oniro™ is a registered trademark of Eclipse Foundation.

|main_project_name| is an `OpenHarmony <https://gitee.com/openharmony>`_ 
compatible open-source project aimed at reducing fragmentation in the
consumer and IoT device industry by providing a common technology that can
power devices irregardless of their make or model. It currently provides the
base of an ecosystem where partners can drive a unified platform aiming at IoT
products. As the ecosystem evolves, the project will tackle a feature-proof
distributed operating system as part of an all-scenario strategy initiative,
adaptable to mobile devices, fitness and health targets, entertainment systems,
and so on. Unlike a legacy operating system that targets a specific device,
|main_project_name| will adopt a distributed architecture design based on a set
of system capabilities. Starting with a set of reference platforms, |main_project_name|
will be flexible enough to target a wide range of devices that accompany users'
daily life.

.. toctree::
   :caption: Introduction
   :maxdepth: 2
   
   overview/oniroproject-vision
 
.. toctree::
   :caption: Quick Start
   :maxdepth: 2

   oniro/oniro-quick-build

.. toctree::
   :caption: Oniro Project Blueprints
   :maxdepth: 2

   Oniro Project Blueprints <https://docs.oniroproject.org/projects/blueprints>
   
.. toctree::
   :caption: Build System Guide
   :maxdepth: 2

   Overview <oniro/yocto-build-system/index>
   Creating Workspace <oniro/repo-workspace>
   oniro/build-flavours/index
   oniro/oniro-build/index
   oniro/os/index
   oniro/ci/index
   oniro/supported-images
   oniro/hardware-support/index
   building-project-documentation

.. toctree::
   :caption: Supported Technologies
   :maxdepth: 2

   meta-openharmony/supported-technologies/openharmony
   oniro/supported-technologies/openthread
   oniro/supported-technologies/containers
   oniro/supported-technologies/modbus

.. toctree::
   :caption: Supported Toolchains
   :maxdepth: 2

   oniro/toolchains

.. toctree::
   :caption: Troubleshoot
   :maxdepth: 2

   oniro/fallback-devices-support
   oniro/debug-mode

.. toctree::
   :caption: Contribute
   :maxdepth: 2

   contributing/index
   ci/index

.. toctree::
   :caption: Community
   :maxdepth: 2

   code-of-conduct
   community-chat-platform
   
.. toctree::
   :caption: Policies and Compliance
   :maxdepth: 2
   
   ip-policy/index
   security/index
   
.. toctree::
   :caption: Releases
   :maxdepth: 2
   
   releases/index
