# SPDX-FileCopyrightText: Huawei Inc.
#
# SPDX-License-Identifier: Apache-2.0

BUILD_DIR ?= build

.PHONY: all
all: $(BUILD_DIR)

.PHONY: clean
clean:
	rm -rf build

SRC_DIR ?= .
$(BUILD_DIR): $(filter-out README.rst contributing/project.rst experimental/%,$(shell find $(SRC_DIR) -name '*.rst'))
	sphinx-build -W $(SRC_DIR) $(BUILD_DIR)
